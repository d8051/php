#!/bin/bash

read -ra extension_array <<< "${EXTENSIONS}"

echo "There are ${#extension_array[*]} extensions that will be enabled."

for ext in "${extension_array[@]}";
do
    sed -i "s/;extension=${ext}/extension=${ext}/" /etc/php/php.ini
    
    for FILEPATH in /etc/php/conf.d/*.ini; 
    do 
        sed -i "s/;extension=${ext}/extension=${ext}/" $FILEPATH
    done

    echo "extension ${ext} enabled!"
done
